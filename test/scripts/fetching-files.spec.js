import { describe, before, after, it } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";

import "should";
import "should-http";

const Test = await SDT( Core );

describe( "On fetching files Hitchy plugin static", () => {
	const ctx = {};

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		projectFolder: "../project",
		plugin: true,
	} ) );

	it( "is delivering selected Javascript file", async() => {
		const res = await ctx.get( "/files/test.js" );

		res.should.have.status( 200 );
		res.headers["content-type"].should.be.equal( "application/javascript" );
		res.body.toString().trim().should.be.equal( '"test.js";' );
	} );

	it( "is delivering selected HTML file", async() => {
		const res = await ctx.get( "/files/test.html" );

		res.should.have.status( 200 );
		res.headers["content-type"].should.be.equal( "text/html" );
		res.body.toString().trim().should.be.equal( "test.html" );
	} );

	it( "is delivering selected XML file", async() => {
		const res = await ctx.get( "/files/data.xml" );

		res.should.have.status( 200 );
		res.headers["content-type"].should.be.equal( "application/xml" );
		res.body.toString().trim().should.be.equal( "data.xml" );
	} );

	it( "is delivering selected CSS file (using static provider overlapping w/ another one)", async() => {
		const res = await ctx.get( "/media/sub/test.css" );

		res.should.have.status( 200 );
		res.headers["content-type"].should.be.equal( "text/css" );
		res.text.trim().should.be.equal( "this is test.css" );
	} );

	it( "is failing on accessing missing file", async() => {
		const res = await ctx.get( "/media/sub/missing.css" );

		res.should.have.status( 404 );
		res.headers["content-type"].should.match( /text\/plain\b/ );
		res.text.trim().should.be.equal( "no such file" );
	} );

	it( "is delivering configured fallback on requesting missing file", async() => {
		let res = await ctx.get( "/with-fallback/test.css" );

		res.should.have.status( 200 );
		res.headers["content-type"].should.be.equal( "text/css" );
		res.text.trim().should.be.equal( "this is test.css" );

		res = await ctx.get( "/with-fallback/missing.css" );

		res.should.have.status( 200 );
		res.headers["content-type"].should.be.equal( "image/gif" );
		res.body.toString().trim().should.be.equal( "an-image.gif" );
	} );
} );
