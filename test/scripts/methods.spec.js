import { describe, before, after, it } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";

import "should";
import "should-http";

const Test = await SDT( Core );

describe( "Hitchy plugin static", () => {
	const ctx = {};

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		projectFolder: "../project",
		plugin: true,
	} ) );

	it( "accepts GET method for fetching, only", async() => {
		const [ get, post, put, del ] = await Promise.all( [
			ctx.get( "/files/test.html" ),
			ctx.post( "/files/test.html" ),
			ctx.put( "/files/test.html" ),
			ctx.delete( "/files/test.html" ),
		] );

		get.should.have.status( 200 );
		get.body.toString().trim().should.be.equal( "test.html" );

		post.should.have.status( 400 );
		post.body.toString().trim().should.not.be.equal( "test.html" );

		put.should.have.status( 400 );
		put.body.toString().trim().should.not.be.equal( "test.html" );

		del.should.have.status( 400 );
		del.body.toString().trim().should.not.be.equal( "test.html" );
	} );

	it( "supports HEAD method for testing", async() => {
		const [ html, js ] = await Promise.all( [
			ctx.request( "HEAD", "/files/test.html" ),
			ctx.request( "HEAD", "/files/test.js" ),
		] );

		html.should.have.status( 200 );
		html.body.toString().should.be.empty();
		html.headers.should.not.have.property( "content-type" );
		html.headers.should.have.property( "content-length" );
		html.headers.should.have.property( "last-modified" );

		js.should.have.status( 200 );
		js.body.toString().should.be.empty();
		js.headers.should.not.have.property( "content-type" );
		js.headers.should.have.property( "content-length" );
		js.headers.should.have.property( "last-modified" );
	} );
} );
