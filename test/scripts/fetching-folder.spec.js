import { resolve } from "node:path";
import { fileURLToPath } from "node:url";

import { describe, before, after, it } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";

import "should";
import "should-http";

const Test = await SDT( Core );

describe( "On fetching folders Hitchy plugin static", () => {
	const ctx = {};

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		projectFolder: "../project",
		plugin: true,
	} ) );

	it( "rejects requests addressing folder w/o index.html", async() => {
		const res = await ctx.get( "/files/section-a" );

		res.should.have.status( 403 );
	} );

	it( "delivers content of folder's index.html file", async() => {
		const [ resA, resB ] = await Promise.all( [
			ctx.get( "/media" ),
			ctx.get( "/media/sub/media/" ),
		] );

		resA.should.have.status( 200 );
		resA.text.trim().should.be.equal( "this is /media/index.html" );

		resB.should.have.status( 200 );
		resB.text.trim().should.be.equal( "this is /media/index.html" );
	} );
} );


describe( "On fetching folders w/ redirection to index documents enabled Hitchy plugin static", () => {
	const ctx = {};

	before( Test.before( ctx, {
		projectFolder: resolve( fileURLToPath( import.meta.url ), "../../project" ),
		pluginsFolder: resolve( fileURLToPath( import.meta.url ), "../../.." ),
		files: {
			"config/static.cjs": `
exports.static = [
	{
		prefix: "/files",
		folder: "files",
		redirect: true,                            // <-- this is different
	},
	{
		prefix: "/media",
		folder: "./files/section-a/media/",
		redirect: true,                            // <-- this is different
	},
	{
		prefix: "/media/sub",
		folder: "./files/section-a",
		redirect: true,                            // <-- this is different
	},
	{
		prefix: "/with-fallback",
		folder: "./files/section-a/",
		fallback: "media/an-image.gif",
	},
];
`,
		},
	} ) );

	after( Test.after( ctx ) );

	it( "rejects requests addressing folder w/o index.html", async() => {
		const res = await ctx.get( "/files/section-a" );

		res.should.have.status( 403 );
	} );

	it( "redirects to index.html on requesting folder w/ index.html", async() => {
		const [ resA, resB ] = await Promise.all( [
			ctx.get( "/media" ),
			ctx.get( "/media/sub/media/" ),
		] );

		resA.should.have.status( 301 );
		resA.headers["location"].should.be.equal( "/media/index.html" );

		resB.should.have.status( 301 );
		resB.headers["location"].should.be.equal( "/media/sub/media/index.html" );
	} );
} );


describe( "On fetching folders w/ fallback defined Hitchy plugin static", () => {
	const ctx = {};

	before( Test.before( ctx, {
		projectFolder: resolve( fileURLToPath( import.meta.url ), "../../project" ),
		pluginsFolder: resolve( fileURLToPath( import.meta.url ), "../../.." ),
		files: {
			"files/section-a/another-sub/custom.css": "this is custom.css",   // <-- this is different
			"config/static.cjs": `
exports.static = [
	{
		prefix: "/files",
		folder: "files",
		fallback: "section-a/test.css",                      // <-- this is different
	},
	{
		prefix: "/media",
		folder: "./files/section-a/media/",
		fallback: "section-a/test.css",                      // <-- this is different
	},
	{
		prefix: "/media/sub",
		folder: "./files/section-a",
		fallback: "section-a/test.css",                      // <-- this is different
	},
	{
		prefix: "/with-fallback",
		folder: "./files/section-a/",
		fallback: "media/an-image.gif",
	},
];
`,
		},
	} ) );

	after( Test.after( ctx ) );

	it( "delivers content of defined fallback on folder w/o index.html", async() => {
		const [ resA, resB, resC ] = await Promise.all( [
			ctx.get( "/files/section-a" ),
			ctx.get( "/files/section-a/another-sub" ),
			ctx.get( "/files/section-a/another-sub/custom.css" ),
		] );

		resA.should.have.status( 200 );
		resA.text.trim().should.be.equal( "this is test.css" );

		resB.should.have.status( 200 );
		resB.text.trim().should.be.equal( "this is test.css" );

		resC.should.have.status( 200 );
		resC.text.trim().should.be.equal( "this is custom.css" );
	} );

	it( "delivers content of folder's index.html file", async() => {
		const [ resA, resB ] = await Promise.all( [
			ctx.get( "/media" ),
			ctx.get( "/media/sub/media/" ),
		] );

		resA.should.have.status( 200 );
		resA.text.trim().should.be.equal( "this is /media/index.html" );

		resB.should.have.status( 200 );
		resB.text.trim().should.be.equal( "this is /media/index.html" );
	} );
} );


describe( "On fetching folders w/ fallback defined and redirection to a folder's index.html enabled, Hitchy plugin static", () => {
	const ctx = {};

	before( Test.before( ctx, {
		projectFolder: resolve( fileURLToPath( import.meta.url ), "../../project" ),
		pluginsFolder: resolve( fileURLToPath( import.meta.url ), "../../.." ),
		files: {
			"files/section-a/another-sub/custom.css": "this is custom.css",   // <-- this is different
			"config/static.cjs": `
exports.static = [
	{
		prefix: "/files",
		folder: "files",
		fallback: "section-a/test.css",                      // <-- this is different
		redirect: true,                                      // <-- this is different
	},
	{
		prefix: "/media",
		folder: "./files/section-a/media/",
		fallback: "section-a/test.css",                      // <-- this is different
		redirect: true,                                      // <-- this is different
	},
	{
		prefix: "/media/sub",
		folder: "./files/section-a",
		fallback: "section-a/test.css",                      // <-- this is different
		redirect: true,                                      // <-- this is different
	},
	{
		prefix: "/with-fallback",
		folder: "./files/section-a/",
		fallback: "media/an-image.gif",
	},
];
`,
		},
	} ) );

	after( Test.after( ctx ) );

	it( "delivers content of defined fallback on folder w/o index.html", async() => {
		const [ resA, resB, resC ] = await Promise.all( [
			ctx.get( "/files/section-a" ),
			ctx.get( "/files/section-a/another-sub" ),
			ctx.get( "/files/section-a/another-sub/custom.css" ),
		] );

		resA.should.have.status( 200 );
		resA.text.trim().should.be.equal( "this is test.css" );

		resB.should.have.status( 200 );
		resB.text.trim().should.be.equal( "this is test.css" );

		resC.should.have.status( 200 );
		resC.text.trim().should.be.equal( "this is custom.css" );
	} );

	it( "redirects to index.html on requesting folder w/ index.html", async() => {
		const [ resA, resB ] = await Promise.all( [
			ctx.get( "/media" ),
			ctx.get( "/media/sub/media/" ),
		] );

		resA.should.have.status( 301 );
		resA.headers["location"].should.be.equal( "/media/index.html" );

		resB.should.have.status( 301 );
		resB.headers["location"].should.be.equal( "/media/sub/media/index.html" );
	} );
} );
