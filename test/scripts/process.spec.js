import { describe, it, before, after } from "mocha";
import SDT from "@hitchy/server-dev-tools";
import Core from "@hitchy/core/sdt.js";

import "should";

const Test = await SDT( Core );

function myFilter( url, pathname, isRequested ) { // eslint-disable-line no-unused-vars
	if ( url.match( /\bsecret\.txt/ ) ) {
		throw new this.services.HttpException( 403, "this one is hidden" );
	}
};

function myProcessor( url, pathname, stream ) {
	if ( url.match( /\bpublic\.txt/ ) ) {
		return stream;
	}

	if ( url.match( /\bpublic\.json/ ) ) {
		this.response.json( { different: true } );
		stream.destroy();
		return null;
	}

	const transformed = new ( require( "stream" ).Transform )( {
		transform( chunk, encoding, callback ) {
			callback( null, chunk.toString( "utf8" ).toUpperCase() );
		}
	} );

	stream.pipe( transformed );

	return transformed;
};

describe( "Filter callback", () => {
	const ctx = {};

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		plugin: true,
		files: {
			"config/static.cjs": `exports.static = [ { prefix: "/files", folder: "files", filter: ${myFilter} } ];`,
			"files/public.txt": `hello world`,
			"files/secret.txt": `do not expose`,
		},
	} ) );

	it( "is obeyed", async() => {
		let res = await ctx.get( "/files/public.txt" );

		res.statusCode.should.be.equal( 200 );
		res.text.should.be.equal( "hello world" );

		res = await ctx.get( "/files/secret.txt" );

		res.statusCode.should.be.equal( 403 );
		res.text.should.be.equal( "accessing file or folder forbidden" );
	} );
} );

describe( "Process callback", () => {
	const ctx = {};

	after( Test.after( ctx ) );
	before( Test.before( ctx, {
		plugin: true,
		files: {
			"config/static.cjs": `exports.static = [ { prefix: "/files", folder: "files", filter: ${myFilter}, process: ${myProcessor} } ];`,
			"files/public.txt": `hello world`,
			"files/public.json": `hello world?`,
			"files/processed.txt": `this is uppercase!`,
			"files/secret.txt": `do not expose`,
		},
	} ) );

	it( "is obeyed", async() => {
		let res = await ctx.get( "/files/public.txt" );

		res.statusCode.should.be.equal( 200 );
		res.text.should.be.equal( "hello world" );

		res = await ctx.get( "/files/secret.txt" );

		res.statusCode.should.be.equal( 403 );
		res.text.should.be.equal( "accessing file or folder forbidden" );

		res = await ctx.get( "/files/processed.txt" );

		res.statusCode.should.be.equal( 200 );
		res.text.should.be.equal( "THIS IS UPPERCASE!" );

		res = await ctx.get( "/files/public.json" );

		res.statusCode.should.be.equal( 200 );
		res.data.different.should.be.true();
	} );
} );
