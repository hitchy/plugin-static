"use strict";

exports.static = [
	{
		prefix: "/files",
		folder: "files",
	},
	{
		prefix: "/media",
		folder: "./files/section-a/media/",
	},
	{
		prefix: "/media/sub",
		folder: "./files/section-a",
	},
	{
		prefix: "/with-fallback",
		folder: "./files/section-a/",
		fallback: "media/an-image.gif",
	},
];
